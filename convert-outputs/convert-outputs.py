#!/usr/bin/env python

import os
import re
import pandas as pd

input_path = "../build/output"

# x.wifi.[model], y.wifi.[model], z.wifi.[model], x_global.wifi.[model], y_global.wifi.[model], floor.wifi[model], x.wifi2.[model], y.wifi2.[model], z.wifi2.[model], x_global.wifi2.[model], y_global.wifi2.[model], floor.wifi2[model], x.gsm.[model], y.gsm.[model], z.gsm.[model], x_global.gsm.[model], y_global.gsm.[model], floor.gsm[model], x.gsm+wifi.[model], y.gsm+wifi.[model], z.gsm+wifi.[model], x_global.gsm+wifi.[model], y_global.gsm+wifi.[model], floor.gsm+wifi[model]


for root, dirs, files in os.walk(input_path):
    for file_name in files:
        mf = re.search(r"(?P<file_name>\b.+predictor_test_set_results\.csv\b)", file_name)
        m = re.search(r"/build predictor for (?P<building>.+) \((?P<building_number>\d+)\) "
                      r"of size \[(?P<network>.+)\] "
                      r"for (?P<type>\bwifi|gsm\b) "
                      r"(?P<coords>\bglobal|local\b) coords/(?P<date>\d+_\d+)", root)

        if mf is None or m is None:
            continue
        network_data = m.groupdict()
        serie = 3 # assuming test serie
        coords_label = "_global" if network_data['coords'] == 'global' else ''
        model = "autoencoder_" + network_data['network'].replace(' ', '').replace(',', '_')

        output_file = "{0}.{1}.output.csv".format(network_data['building'], model)

        try:
            raw_data = pd.read_csv(os.path.join(root, file_name), header=None)
            print "extracted ", os.path.join(root, file_name)
        except ValueError:
            print "error reading ", os.path.join(root, file_name)
            continue
        data = {
            'building': [network_data['building']] * len(raw_data),
            'serie': [serie] * len(raw_data),
            'x' + coords_label: raw_data[0],
            'y' + coords_label: raw_data[1],
            'z': raw_data[2],
            'floor': map(lambda x: round(x/4, 0), raw_data[2]),
            "x{0}.{1}.{2}".format(coords_label, network_data['type'], model): raw_data[3],
            "y{0}.{1}.{2}".format(coords_label, network_data['type'], model): raw_data[4],
            "z.{0}.{1}".format(network_data['type'], model): raw_data[5],
            "floor.{0}.{1}".format(network_data['type'], model): map(lambda x: round(x/4, 0), raw_data[5])
            }
        data_frame = pd.DataFrame(data, columns=data.keys())
        data_frame.to_csv(os.path.join(root, output_file))