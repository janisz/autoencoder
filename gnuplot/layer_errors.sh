#!/bin/sh
INPUT_FOLDER="../build/output/test/error/"
OUTPUT_FOLDER="plots/"
# training and test error plots
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_0'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_error_0.eps'" errorplot.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_error.eps'" errorplot.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_1'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_error_1.eps'" errorplot.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_2'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_error_2.eps'" errorplot.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_3'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_error_3.eps'" errorplot.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_4'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_error_4.eps'" errorplot.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_predictor'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_error_predictor.eps'" errorplot.gnuplot

# error histograms
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_0'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_hist_0.eps'" histogram.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_hist.eps'" histogram.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_1'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_hist_1.eps'" histogram.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_2'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_hist_2.eps'" histogram.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_3'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_hist_3.eps'" histogram.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_4'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_hist_4.eps'" histogram.gnuplot
gnuplot -e "datapath='"$INPUT_FOLDER"olp_eksport_wifi_error_predictor'; outputpath='"$OUTPUT_FOLDER"olp_eksport_wifi_hist_predictor.eps'" histogram.gnuplot
