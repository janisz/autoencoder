set datafile separator ","


set xyplane 0
set xrange [-60:60]
set yrange [-60:60]
set zrange [-60:60]

set zeroaxis lt 1 lw 2 lc rgb "black"

set grid xtics
set grid ytics

set terminal pngcairo size 1080,1080 enhanced font 'Verdana,10'


set xlabel 'X (m)'
set ylabel 'Y (m)'
set output 'wifiXY.png'
plot "predictor_training_set_error.csv" u 1:2, "predictor_validation_set_error.csv" u 1:2, "predictor_test_set_error.csv" u 1:2

set xlabel 'X (m)'
set ylabel 'Z (m)'
set output 'wifiXZ.png'
plot "predictor_training_set_error.csv" u 1:3, "predictor_validation_set_error.csv" u 1:3, "predictor_test_set_error.csv" u 1:3

set xlabel 'Z (m)'
set ylabel 'Y (m)'
set output 'wifiZY.png'
plot "predictor_training_set_error.csv" u 3:2, "predictor_validation_set_error.csv" u 3:2, "predictor_test_set_error.csv" u 3:2
set datafile separator ","

set title 'WiFi Predictor'
set xlabel 'X (m)'
set ylabel 'Y (m)'
set zlabel 'Z (m)'
set output 'wifi.png'

splot "predictor_training_set_error.csv", "predictor_validation_set_error.csv", "predictor_test_set_error.csv"
