# usage: gnuplot -e "datapath='../output/trainingError/data.train.csv'; outputpath='olp_eksport_wifi_error.eps'" errorplot.gnuplot
# pass filename as parameter
if (!exists("datapath")) datapath='../build/output/test/error/olp_eksport_wifi_error_3'
if (!exists("outputpath")) outputpath='olp_eksport_wifi_3_hist.eps'


set term eps
set output outputpath
set datafile separator ","

n=100 #number of intervals
max=1.0 #max value
min=0.0 #min value
width=(max-min)/n #interval width

#function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0
set xrange [min:max]
set yrange [0:]

#to put an empty boundary around the
#data inside an autoscaled graph.
set offset graph 0.05,0.05,0.05,0.0
set xtics min,(max-min)/5,max

set boxwidth width*0.9
set style fill solid 0.5 #fillstyle
set tics out nomirror

set xlabel "Error"
set ylabel "Number of  "


#count and plot
#plot "data.dat" u  smooth freq w boxes lc rgb"green" notitle

plot datapath using (hist($2,width)):(1.0) smooth freq with boxes lc rgb"green" notitle