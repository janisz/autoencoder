# usage: gnuplot -e "datapath='../output/trainingError/data.train.csv'; outputpath='olp_eksport_wifi_error.eps'" errorplot.gnuplot
# pass filename as parameter
if (!exists("datapath")) datapath='../build/output/test/error/olp_eksport_wifi_error'
if (!exists("outputpath")) outputpath='olp_eksport_wifi_error.eps'

set xlabel "Number of iterations"
set ylabel "Error [m]"

set term png
set output outputpath

set datafile separator ","
set key top right

set logscale y

plot datapath using (column(0))*100:2 title 'training error' with lines ls 1, \
     datapath using (column(0))*100:3 title 'test error' with lines ls 2