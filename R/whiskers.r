args <- commandArgs(trailingOnly = TRUE)
print('Usage: [input file (csv)] [output file (png)]')

inputFile <- args[1]
outputFile <- args[2]

inputFile
outputFile

test <- read.csv(file=inputFile, header=T, sep=",")

pdf(outputFile, width=15, height = 10)
attach(mtcars)
layout(matrix(c(1,2,3,4,5,6), 3, 2, byrow = TRUE),  heights=c(2,2,3))

o = c(1,2,3,5,11) + 2
bxp.data = list( stats = t(data.matrix(test[,o])), n = rep(1,nrow(test)))
bxp(bxp.data, horizontal=F, las=2, main = "Absolute value of error on X", xlab = "", ylab = "Abs err [m]")
o = c(1,2,3,4,5) + 6 + 2
bxp.data = list( stats = t(data.matrix(test[,o])), n = rep(1,nrow(test)))
bxp(bxp.data, horizontal=F, las=2, main = "90%-98% of absoulute value of error on X", xlab = "", ylab = "Abs err [m]")

o = c(1,2,3,5,11) + 13
bxp.data = list( stats = t(data.matrix(test[,o])), n = rep(1,nrow(test)))
bxp(bxp.data, horizontal=F, las=2, main = "Absolute value of error on Y", xlab = "", ylab = "Abs err [m]")
o = c(1,2,3,4,5) + 6 + 13
bxp.data = list( stats = t(data.matrix(test[,o])), n = rep(1,nrow(test)))
bxp(bxp.data, horizontal=F, las=2, main = "90%-98% of absoulute value of error on Y", xlab = "", ylab = "Abs err [m]")

o = c(1,2,3,5,11) + 5 + 6 + 13
bxp.data = list( stats = t(data.matrix(test[,o])), n = rep(1,nrow(test)), names = test[,2], par(mar = c(17, 5, 4, 2)+ 0.1))
bxp(bxp.data, horizontal=F, las=2, main = "Absolute value of error on Z", xlab = "", ylab = "Abs err [m]")
o = c(1,2,3,4,5) + 6 + 5 + 6 + 13
bxp.data = list( stats = t(data.matrix(test[,o])), n = rep(1,nrow(test)), names = test[,2], par(mar = c(17, 5, 4, 2)+ 0.1))
bxp(bxp.data, horizontal=F, las=2, main = "90%-98% of absoulute value of error on Z", xlab = "", ylab = "Abs err [m]")

dev.off()