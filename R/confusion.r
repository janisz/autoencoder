args <- commandArgs(trailingOnly = TRUE)
print('Usage: [input file (csv)] [output file (csv)]')

inputFile <- args[1]
outputFile <- args[2]

inputFile
outputFile

test <- read.csv(file=inputFile, header=FALSE, sep=",")

confusion <- table(round(test$V2/4), round(test$V1/4))

write.csv(confusion, file = outputFile)
