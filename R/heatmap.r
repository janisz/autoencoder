library(hexbin)
library(gridExtra)

options(echo=TRUE)
args <- commandArgs(trailingOnly = TRUE)
print('Usage: [input file (csv)] [output file (png)] [buckets count]')
inputFile <- args[1]
outputFile <- args[2]

inputFile

pdf(outputFile, width=15, height=5)

test <- read.csv(file=inputFile, header=FALSE, sep=",")


p1 <- hexbinplot(test$V1 ~ test$V2, xlab="Error of X [m]", ylab="Error of Y [m]", xlim=c(-40, 40), ylim=c(-40, 40), aspect=1)
p2 <- hexbinplot(test$V2 ~ test$V3, xlab="Error of Y [m]", ylab="Error of Z [m]", xlim=c(-40, 40), ylim=c(-40, 40), aspect=1)
p3 <- hexbinplot(test$V1 ~ test$V3, xlab="Error of X [m]", ylab="Error of Z [m]", xlim=c(-40, 40), ylim=c(-40, 40), aspect=1)

grid.arrange(p1, p2, p3,ncol=3)
