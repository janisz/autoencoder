args <- commandArgs(trailingOnly = TRUE)
print('Usage: [input file (csv)] [output file (csv)]')

inputFile <- args[1]
outputFile <- args[2]

inputFile
outputFile

test <- read.csv(file=inputFile, header=FALSE, sep=",")

accuracy <- nrow(subset(test, round(V3/4) == round(V6/4)))/nrow(test)

write.table(accuracy, file = outputFile)
