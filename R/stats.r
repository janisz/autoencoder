args <- commandArgs(trailingOnly = TRUE)
print('Usage: [input file (csv)] [output file (csv)] [output file (png)] [buckets count]')

inputFile <- args[1]
outputFile <- args[2]

inputFile
outputFile

# set up custom class
methods::setClass("nan.is.0")
methods::setAs("character", "nan.is.0",
      function(from) replace(as.numeric(from), from == "NaN", 0))

test <- read.csv(file=inputFile, header=FALSE, sep=",", colClasses = "nan.is.0")
configuration <- unlist(strsplit(unlist(strsplit(inputFile, "\\["))[2], "\\]"))[1]

summary(abs(test$V1))
summary(abs(test$V2))
summary(abs(test$V3))

stats <- data.frame(configuration, matrix(c(
  summary(abs(test$V1)), quantile(test$V1, 0.9), quantile(test$V1, 0.92), quantile(test$V1, 0.94), quantile(test$V1, 0.96), quantile(test$V1, 0.98),
  summary(abs(test$V2)), quantile(test$V2, 0.9), quantile(test$V2, 0.92), quantile(test$V2, 0.94), quantile(test$V2, 0.96), quantile(test$V2, 0.98),
  summary(abs(test$V3)), quantile(test$V3, 0.9), quantile(test$V3, 0.92), quantile(test$V3, 0.94), quantile(test$V3, 0.96), quantile(test$V3, 0.98)
  ), ncol=33, byrow=TRUE))
colnames(stats) <- c("File", "X - Min.", "X - 1st Qu.", "X - Median", "X - Mean", "X - 3rd Qu.", "X - Max.", "X - 90%", "X - 92%", "X - 94%", "X - 96%", "X - 98%",
                             "Y - Min.", "Y - 1st Qu.", "Y - Median", "Y - Mean", "Y - 3rd Qu.", "Y - Max.", "Y - 90%", "Y - 92%", "Y - 94%", "Y - 96%", "Y - 98%",
                             "Z - Min.", "Z - 1st Qu.", "Z - Median", "Z - Mean", "Z - 3rd Qu.", "Z - Max.", "Z - 90%", "Z - 92%", "Z - 94%", "Z - 96%", "Z - 98%")
write.csv(stats, file = outputFile)
