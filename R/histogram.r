options(echo=TRUE)
args <- commandArgs(trailingOnly = TRUE)
print('Usage: [input file (csv)] [output file (png)] [buckets count]')

inputFile <- args[1]
outputFile <- args[2]
buckets <- strtoi(args[3])

inputFile

pdf(outputFile)
attach(mtcars)
par(mfrow=c(3,1))

test <- read.csv(file=inputFile, header=FALSE, sep=",")

hist(test$V1, buckets, col="lightblue", xlab="Error of X [m]", ylab="Frequency",main="", xlim=c(-40,40))
hist(test$V2, buckets, col="lightblue", xlab="Error of Y [m]", ylab="Frequency",main="", xlim=c(-40,40))
hist(test$V3, buckets, col="lightblue", xlab="Error of Z [m]", ylab="Frequency",main="", xlim=c(-20,20))
