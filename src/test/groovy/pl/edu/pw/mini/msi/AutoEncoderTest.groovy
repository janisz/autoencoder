package pl.edu.pw.mini.msi

import org.encog.ml.data.MLData
import org.encog.ml.data.MLDataSet
import org.encog.ml.data.basic.BasicMLData
import org.encog.ml.data.basic.BasicMLDataSet
import org.encog.neural.networks.BasicNetwork
import org.junit.Rule
import org.junit.rules.TestName
import spock.lang.Specification

class AutoEncoderTest extends Specification {

    @Rule TestName name = new TestName()

    Visualizer visualizer = new Visualizer();
    NetworkFactory networkFactory = new NetworkFactory()
    FileManager fileManager;
    String outputDir;

    def setup() {
        outputDir = "build/output/${name.methodName}/${new Date().format('yyyyMMdd_HHmmss/')}"
        fileManager = new FileManager("$outputDir/test/error/", "$outputDir/test/visualisation/")
    }

    def "should build Autoencoder Structure"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 1000)
        AutoEncoder autoEncoder = new AutoEncoder([5, 3, 2, 1], new double[2][7], new double[2][7], networkFactory, trainer, visualizer, fileManager, "1");
        expect:
        9 == autoEncoder.network.layerCount
        '7->TANH->5->TANH->3->TANH->2->TANH->1->TANH->2->TANH->3->TANH->5->TANH->7' == autoEncoder.network.getFactoryArchitecture()
    }

    def "should compute next layer training data"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 1000)
        AutoEncoder autoEncoder = new AutoEncoder([5, 3, 2, 1], new double[2][7], new double[2][7], networkFactory, trainer, visualizer, fileManager, "2");
        BasicMLDataSet trainingData = new BasicMLDataSet([[1], [-1]] as double[][], [[-1], [1]] as double[][])
        BasicMLDataSet validationData = new BasicMLDataSet([[1], [-1]] as double[][], [[-1], [1]] as double[][])
        when:
        BasicNetwork net = autoEncoder.trainLayerNetwork(0, inputSize, trainingData, validationData)
        then:
        closeEnough([1], net.compute(new BasicMLData([-1] as double[])), 0.01)
        closeEnough([-1], net.compute(new BasicMLData([1] as double[])), 0.01)
        inputSize == autoEncoder.getNextTrainingData(trainingData, net).inputSize

        where:
        inputSize << [1, 3, 5]
    }

    def "should build autoencoder"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 1000)
        AutoEncoder autoEncoder = new AutoEncoder([1], [[-1], [0], [1]] as double[][], [[-1], [0], [1]] as double[][], networkFactory, trainer, visualizer, fileManager, "2");
        when:
        autoEncoder.buildAutoEncoder()
        then:
        println autoEncoder.network.factoryArchitecture
        closeEnough([-1], autoEncoder.compute(new BasicMLData([-1] as double[])), 0.01)
        closeEnough([0], autoEncoder.compute(new BasicMLData([0] as double[])), 0.01)
        closeEnough([1], autoEncoder.compute(new BasicMLData([1] as double[])), 0.01)

    }

    def "should build autoencoder 1"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 1000)
        NetworkFactory networkFactory = new NetworkFactory(seed: 1024)
        AutoEncoder autoEncoder = new AutoEncoder([2, 1], [[-1], [0], [1]] as double[][], [[-1], [0], [1]] as double[][], networkFactory, trainer, visualizer, fileManager, "3");
        when:
        autoEncoder.buildAutoEncoder()
        then:
        closeEnough([-1], autoEncoder.compute(new BasicMLData([-1] as double[])), 0.01)
        closeEnough([0], autoEncoder.compute(new BasicMLData([0] as double[])), 0.01)
        closeEnough([1], autoEncoder.compute(new BasicMLData([1] as double[])), 0.01)

    }

    def "should build bigger autoencoder"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 1000)
        NetworkFactory networkFactory = new NetworkFactory(seed: 99)
        AutoEncoder autoEncoder = new AutoEncoder([3, 2], [[1, 0, 0]] as double[][], [[1, 0, 0]] as double[][], networkFactory, trainer, visualizer, fileManager, "4");
        when:
        autoEncoder.buildAutoEncoder()
        then:
        println autoEncoder.network.factoryArchitecture
        closeEnough([1, 0, 0], autoEncoder.compute(new BasicMLData([1, 0, 0] as double[])), 0.25)
    }

    def "should build bigger autoencoder with multiple values"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.05, 100, 1000)
        NetworkFactory networkFactory = new NetworkFactory(seed: 100023)
        AutoEncoder autoEncoder = new AutoEncoder([5, 6], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], networkFactory, trainer, visualizer, fileManager, "5");
        when:
        autoEncoder.buildAutoEncoder()
        then:
        autoEncoder.network.factoryArchitecture == '3->TANH->5->TANH->6->TANH->5->TANH->3'
        closeEnough([1, 0, 0], autoEncoder.compute(new BasicMLData([1, 0, 0] as double[])), 0.01)
        closeEnough([0, 0, 1], autoEncoder.compute(new BasicMLData([0, 0, 1] as double[])), 0.01)
        closeEnough([0, -1, 0], autoEncoder.compute(new BasicMLData([0, -1, 0] as double[])), 0.01)
    }

    def "should build bigger autoencoder with MLDataSet with multiple values"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.05, 100, 1000)
        NetworkFactory networkFactory = new NetworkFactory(seed: 100023)
        MLDataSet dataSet = new BasicMLDataSet([[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][],
                                                [[1], [-1], [0]] as double[][])
        AutoEncoder autoEncoder = new AutoEncoder([5, 6], dataSet, dataSet, networkFactory, trainer, visualizer, fileManager, "5");
        when:
        autoEncoder.buildAutoEncoder()
        then:
        autoEncoder.network.factoryArchitecture == '3->TANH->5->TANH->6->TANH->5->TANH->3'
        closeEnough([1, 0, 0], autoEncoder.compute(new BasicMLData([1, 0, 0] as double[])), 0.01)
        closeEnough([0, 0, 1], autoEncoder.compute(new BasicMLData([0, 0, 1] as double[])), 0.01)
        closeEnough([0, -1, 0], autoEncoder.compute(new BasicMLData([0, -1, 0] as double[])), 0.01)

        when:
        autoEncoder.pretrainPredictor(dataSet, dataSet)
        def predictor = autoEncoder.predictor

        then:
        predictor.factoryArchitecture == '3->TANH->5->TANH->6->TANH->1'
        closeEnough([1], predictor.compute(new BasicMLData([1, 0, 0] as double[])), 0.01)
        closeEnough([0], predictor.compute(new BasicMLData([0, 0, 1] as double[])), 0.01)
        closeEnough([-1], predictor.compute(new BasicMLData([0, -1, 0] as double[])), 0.01)
    }

    def "should train autoencoder"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 10000)
        NetworkFactory networkFactory = new NetworkFactory(seed: 1000023)
        AutoEncoder autoEncoder = new AutoEncoder([3, 2, 1], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], networkFactory, trainer, visualizer, fileManager, "6");
        when:
        autoEncoder.train()
        then:
        println autoEncoder.network.factoryArchitecture
        closeEnough([1, 0, 0], autoEncoder.compute(new BasicMLData([1, 0, 0] as double[])), 0.01)
        closeEnough([0, 0, 1], autoEncoder.compute(new BasicMLData([0, 0, 1] as double[])), 0.01)
        closeEnough([0, -1, 0], autoEncoder.compute(new BasicMLData([0, -1, 0] as double[])), 0.02)
    }

    def "should get encoder"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 10000)
        NetworkFactory networkFactory = new NetworkFactory(seed: 1000023)
        AutoEncoder autoEncoder = new AutoEncoder([3, 2, 1], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], networkFactory, trainer, visualizer, fileManager, "7")
        autoEncoder.train()
        expect:
        '3->TANH->3->TANH->2->TANH->1' == autoEncoder.getEncoder().getFactoryArchitecture()
    }

    def "should get predictor"() {
        given:
        Trainer trainer = new Trainer(0.01, 0.1, 100, 10000)
        NetworkFactory networkFactory = new NetworkFactory(seed: 1000023)
        AutoEncoder autoEncoder = new AutoEncoder([3, 2], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], [[1, 0, 0], [0, -1, 0], [0, 0, 1]] as double[][], networkFactory, trainer, visualizer, fileManager, "7")
        autoEncoder.train()
        when:
        autoEncoder.pretrainPredictor(
                new BasicMLDataSet([[1, 0, 0], [0, -1, 0]] as double[][], [[1, 0], [0, 1]] as double[][]),
                new BasicMLDataSet([[1, 0, 0]] as double[][], [[1, 0]] as double[][]))
        then:
        '3->TANH->3->TANH->2->TANH->2' == autoEncoder.getPredictor().getFactoryArchitecture()
        closeEnough([1, 0], autoEncoder.getPredictor().compute(new BasicMLData([1, 0, 0] as double[])), 0.02)
        closeEnough([0, 1], autoEncoder.getPredictor().compute(new BasicMLData([0, -1, 0] as double[])), 0.02)
        when:
        trainer.trainToError(autoEncoder.getPredictor(),
                new BasicMLDataSet([[1, 0, 0], [0, -1, 0]] as double[][], [[1, 0], [0, 1]] as double[][]),
                new BasicMLDataSet([[1, 0, 0]] as double[][], [[1, 0]] as double[][]))
        then:
        closeEnough([1, 0], autoEncoder.getPredictor().compute(new BasicMLData([1, 0, 0] as double[])), 0.01)
        closeEnough([0, 1], autoEncoder.getPredictor().compute(new BasicMLData([0, -1, 0] as double[])), 0.01)
    }

    boolean closeEnough(List<Double> expected, MLData actual, double epsilon) {
        assert expected.size() == actual.data.length
        for (int i = 0; i < expected.size(); i++) {
            assert (expected[i] + epsilon > actual.data[i]) && (expected[i] - epsilon < actual.data[i])
        }
        return true
    }
}
