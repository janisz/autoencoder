package pl.edu.pw.mini.msi

import org.encog.ml.MLRegression
import org.encog.ml.data.MLDataSet
import org.encog.ml.data.basic.BasicMLData
import org.encog.ml.data.basic.BasicMLDataSet
import org.encog.util.arrayutil.NormalizationAction
import org.encog.util.arrayutil.NormalizedField
import spock.lang.Specification

/**
 * Created by janisz on 22.11.14.
 */
class TesterTest extends Specification {

    def "TestNetwork"() {
        given:
        NormalizedField outputNormalizer = new NormalizedField(NormalizationAction.PassThrough, "output", 1, 0, 1, 0)
        Tester tester = new Tester([outputNormalizer, outputNormalizer, outputNormalizer])
        MLRegression network = Mock(MLRegression)
        network.compute(_) >> new BasicMLData([0.1, 0.2, 0.0] as double[]);
        MLDataSet input = new BasicMLDataSet([[1, 2, 3]] as double[][], [[0.1, 0.1, 0.3]] as double[][])
        expect:
        [[0, -0.1, 0.3]] == tester.testNetwork(network, input)

    }
}
