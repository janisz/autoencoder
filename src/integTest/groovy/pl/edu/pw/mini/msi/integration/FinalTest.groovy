package pl.edu.pw.mini.msi.integration

import groovy.json.JsonBuilder
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.encog.engine.network.activation.ActivationTANH
import org.encog.ml.data.MLDataPair
import org.encog.ml.data.MLDataSet
import org.encog.ml.data.basic.BasicMLDataSet
import org.encog.ml.data.specific.CSVNeuralDataSet
import org.encog.util.arrayutil.NormalizedField
import org.encog.util.obj.SerializeObject
import org.junit.Rule
import org.junit.rules.TestName
import pl.edu.pw.mini.msi.*
import spock.lang.Specification
import spock.lang.Unroll

import java.lang.Void as Should

import static com.google.common.collect.ImmutableList.of
import static java.lang.Math.max
import static org.encog.util.arrayutil.NormalizationAction.Normalize

class FinalTest extends Specification {

    public static final String WIFI = 'wifi'
    public static final String GSM = 'gsm'
    public static final String DATA_PATH = "build/data/2014"
    public static final String MODEL = 'autoencoder'
    public static final String LABELS = 'xyz'
    public static final HashMap<String, String> COORD_SUFFIXES = ['local': '', 'global': '_global']

    @Rule
    TestName name = new TestName()

    Visualizer visualizer = new Visualizer();
    NetworkFactory networkFactory = new NetworkFactory()
    Trainer trainer = new Trainer(0.00001, 0.001, 10, 500)
    FileManager fileManager;
    String outputDir;

    def setup() {
        outputDir = "build/output/${name.methodName}/${new Date().format('yyyyMMdd_HHmmss/')}"
        fileManager = new FileManager("$outputDir/test/error/", "$outputDir/test/visualisation/")
    }

    @Unroll
    Should "build predictor for #buildingName (#buildingId) of size #size for #signal #coordsType coords"() {
        given:

        def trainingDataSetFileName = "${buildingId}_${signal}_training${COORD_SUFFIXES[coordsType]}_${buildingName}.csv"
        def testDataSetFileName = "${buildingId}_${signal}_test${COORD_SUFFIXES[coordsType]}_${buildingName}.csv"
        def validationDataSetFileName = "${buildingId}_${signal}_validation${COORD_SUFFIXES[coordsType]}_${buildingName}.csv"

        def dataFileName = "${buildingId}_${signal}${COORD_SUFFIXES[coordsType]}_${buildingName}.csv"
        def errorData = "${buildingId}_${signal}${COORD_SUFFIXES[coordsType]}_${buildingName}_predictor"
        def serialisedPredictor = "${buildingId}${COORD_SUFFIXES[coordsType]}_${signal}_${buildingName}_predictor.ser"

        def (int inputSize, int outputSize) = getNetworkProperties(trainingDataSetFileName)

        BasicMLDataSet trainingDataSet = new CSVNeuralDataSet("$DATA_PATH/$trainingDataSetFileName", inputSize, outputSize, true)
        BasicMLDataSet validationDataSet = new CSVNeuralDataSet("$DATA_PATH/$validationDataSetFileName", inputSize, outputSize, true)

        NormalizedField inputNormalizer = getInputNormalizer(trainingDataSet)

        List<NormalizedField> idealNormalizers = getIdealNormalizers(trainingDataSet)

        normalizeDataSet(trainingDataSet, inputNormalizer, idealNormalizers)
        normalizeDataSet(validationDataSet, inputNormalizer, idealNormalizers)
        Tester tester = new Tester(idealNormalizers)

        when:
        long start = System.currentTimeMillis()
        AutoEncoder autoEncoder = new AutoEncoder(size, trainingDataSet, validationDataSet, networkFactory,
                trainer, visualizer, fileManager, dataFileName)
        BasicMLDataSet testDataSet = new CSVNeuralDataSet("$DATA_PATH/$testDataSetFileName", inputSize, outputSize, true)
        normalizeDataSet(testDataSet, inputNormalizer, idealNormalizers)

        autoEncoder.train()
        autoEncoder.pretrainPredictor(trainingDataSet, validationDataSet)
        String errorString = trainer.trainToError(autoEncoder.getPredictor(), trainingDataSet, validationDataSet)

        fileManager.saveErrorDataToFile(errorString, errorData)

        long stop = System.currentTimeMillis()

        then:
        def trainingErrorFileName = "${buildingId}_${signal}${COORD_SUFFIXES[coordsType]}_${buildingName}_predictor_training_set_error.csv"
        def validationErrorFileName = "${buildingId}_${signal}${COORD_SUFFIXES[coordsType]}_${buildingName}_predictor_validation_set_error.csv"
        def testErrorFileName = "${buildingId}_${signal}${COORD_SUFFIXES[coordsType]}_${buildingName}_predictor_test_set_error.csv"
        def testResultsFileName = "${buildingId}_${signal}${COORD_SUFFIXES[coordsType]}_${buildingName}_predictor_test_set_results.csv"


        println "Training $signal $serialisedPredictor: ${autoEncoder.getPredictor().factoryArchitecture} took ${stop - start} milis"
        SerializeObject.save(new File(outputDir + serialisedPredictor), autoEncoder.getPredictor());
        trainer.computeErrorOnTestSet(autoEncoder.getPredictor(), testDataSet)
        fileManager.saveErrorDataToCsv(
                tester.testNetwork(autoEncoder.getPredictor(), trainingDataSet),
                trainingErrorFileName)
        fileManager.saveErrorDataToCsv(
                tester.testNetwork(autoEncoder.getPredictor(), validationDataSet),
                validationErrorFileName)

        def testResults = tester.testNetwork(autoEncoder.getPredictor(), testDataSet)
        fileManager.saveErrorDataToCsv(
                testResults,
                testErrorFileName)
        fileManager.saveErrorDataToCsv(
                tester.getComputedResults(autoEncoder.getPredictor(), testDataSet),
                testResultsFileName)

        String[] biases = (1..<(autoEncoder.predictor.flat.layerCounts.length - 1)).collect {
            autoEncoder.predictor.isLayerBiased(it)
        }.toArray()
        List<String> inputHeaders = getInputHeaders(trainingDataSetFileName)
        double errorr = getStdDev(testResults)

        JsonBuilder root = new JsonBuilder()
        LABELS.each { labell ->

            def serialisedPredictorJson = "${buildingName}.${labell}${COORD_SUFFIXES[coordsType]}.${signal}.${MODEL}.json"
            def headerJson = "${buildingName}.${labell}${COORD_SUFFIXES[coordsType]}.${signal}.${MODEL}.header.json"
            def weightsWithLinearOutputNormalisation = (
                    calculateBiasedLinearOutputLayerWeights(labell, idealNormalizers) <<  autoEncoder.predictor.flat.weights.toList()
            ).flatten()

            root {
                activation 'H'
                structure autoEncoder.predictor.flat.layerCounts[1..<(autoEncoder.predictor.flat.layerCounts.length - 1)].reverse()
                weights weightsWithLinearOutputNormalisation
                bias biases
            }
            fileManager.saveDataToFile(root.toPrettyString(), outputDir + serialisedPredictorJson)
            Map<String, Number> linearInputTransform = calculateInputLinearTransform(inputNormalizer.actualLow, inputNormalizer.actualHigh)
            root {
                model MODEL
                building buildingName
                problemType 'R'
                NAvalue 0
                author 'J. Dutkowski, T. Janiszewski'
                label (labell + COORD_SUFFIXES[coordsType])
                year 2014
                train {
                    set trainingDataSetFileName
                    serie of(1)
                }
                validation {
                    set validationDataSetFileName
                    serie of(2)
                }
                test {
                    set testDataSetFileName
                    serie of(3)
                }
                parameters {
                    inputsCount autoEncoder.inputCount
                    outputCount autoEncoder.outputCount
                    hiddenLayers size
                    coach trainer.toMap()
                    method 'ResilientPropagation'
                    activation new ActivationTANH().factoryCode
                }
                inputs inputHeaders
                inputLinearTransform linearInputTransform
                error of(of(errorr))
            }
            fileManager.saveDataToFile(root.toPrettyString(), outputDir + headerJson)
        }

        where:
        signal | coordsType | buildingId | buildingName     | size
        WIFI   | 'global'   | 2          | 'mini'           | [125, 64, 32]
        GSM    | 'global'   | 2          | 'mini'           | [125, 64, 32]
        WIFI   | 'global'   | 2          | 'mini'           | [100, 50]
        GSM    | 'global'   | 2          | 'mini'           | [100, 50]
        WIFI   | 'global'   | 5          | 'gf'             | [125, 64, 32]
        GSM    | 'global'   | 5          | 'gf'             | [125, 64, 32]
        WIFI   | 'global'   | 5          | 'gf'             | [100, 50]
        GSM    | 'global'   | 5          | 'gf'             | [100, 50]
        WIFI   | 'global'   | 6          | 'gg'             | [125, 64, 32]
        GSM    | 'global'   | 6          | 'gg'             | [125, 64, 32]
        WIFI   | 'global'   | 6          | 'gg'             | [100, 50]
        GSM    | 'global'   | 6          | 'gg'             | [100, 50]
        WIFI   | 'global'   | 7          | 'olp_obrzezna'   | [125, 64, 32]
        GSM    | 'global'   | 7          | 'olp_obrzezna'   | [125, 64, 32]
        WIFI   | 'global'   | 7          | 'olp_obrzezna'   | [100, 50]
        GSM    | 'global'   | 7          | 'olp_obrzezna'   | [100, 50]
        WIFI   | 'global'   | 8          | 'kampus'         | [125, 64, 32]
        GSM    | 'global'   | 8          | 'kampus'         | [125, 64, 32]
        WIFI   | 'global'   | 8          | 'kampus'         | [100, 50]
        GSM    | 'global'   | 8          | 'kampus'         | [100, 50]
        GSM    | 'global'   | 9          | 'tul_b'          | [125, 64, 32]
        WIFI   | 'global'   | 9          | 'tul_b'          | [125, 64, 32]
        GSM    | 'global'   | 9          | 'tul_b'          | [100, 50]
        WIFI   | 'global'   | 9          | 'tul_b'          | [100, 50]
        WIFI   | 'global'   | 10         | 'tul_a'          | [125, 64, 32]
        GSM    | 'global'   | 10         | 'tul_a'          | [125, 64, 32]
        WIFI   | 'global'   | 10         | 'tul_a'          | [100, 50]
        GSM    | 'global'   | 10         | 'tul_a'          | [100, 50]
    }

    private List<Number> calculateBiasedLinearOutputLayerWeights(labell, List<NormalizedField> idealNormalizers) {
        def idealNormalizer = idealNormalizers.find { it.name == labell }
        def weightMeaningfulOutput = (idealNormalizer.actualHigh - idealNormalizer.actualLow) / 2
        def biasMeaningfulOutput = (idealNormalizer.actualHigh + idealNormalizer.actualLow) / 2
        if (labell == 'x') {
            return [
                    weightMeaningfulOutput,
                    0,
                    0,
                    biasMeaningfulOutput,
            ]
        } else if (labell == 'y') {
            return [
                    0,
                    weightMeaningfulOutput,
                    0,
                    biasMeaningfulOutput,
            ]
        } else if (labell == 'z') {
            return [
                    0,
                    0,
                    weightMeaningfulOutput,
                    biasMeaningfulOutput,
            ]
        }
    }

    private Map<String, Number> calculateInputLinearTransform(double actualLow, double actualHigh) {
        //http://wolfr.am/57Lsnfcm
        Map<String, Number> transformer = ['a': 0, b: '0']
        try {
            transformer = [
                    'a': -2 / (actualLow - actualHigh),
                    'b': (actualLow + actualHigh) / (actualLow - actualHigh)
            ]
        } finally {
            return transformer
        }
    }

    private double getStdDev(Collection<List<Double>> testResults) {
        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (List<Double> diff : testResults) {
            for (Double value : diff) {
                stats.addValue(value)
            }
        }
        return stats.standardDeviation
    }

    private List getNetworkProperties(GString trainingDataSetFileName) {
        String headers = new File("$DATA_PATH/$trainingDataSetFileName").withReader { it.readLine() }
        def outputSize = 3
        def inputSize = headers.findAll(',').size() - outputSize + 1
        [inputSize, outputSize]
    }

    private List<String> getInputHeaders(GString trainingDataSetFileName) {
        String headers = new File("$DATA_PATH/$trainingDataSetFileName").withReader { it.readLine() }
        def outputSize = 3
        def headersList = headers.split(',')
        return headersList[1..<headersList.size() - outputSize]
    }

    private void normalizeDataSet(MLDataSet dataSet, NormalizedField inputNormalizer, List<NormalizedField> idealNormalizers) {
        for (MLDataPair dataPair : dataSet) {
            for (int i = 0; i < dataPair.getIdeal().size(); i++) {
                dataPair.getIdeal().setData(i, idealNormalizers[i].normalize(dataPair.getIdeal().getData(i)))
            }
            for (int i = 0; i < dataPair.getInput().size(); i++) {
                dataPair.getInput().setData(i, inputNormalizer.normalize(dataPair.getInput().getData(i)))
            }
        }
    }

    private NormalizedField getInputNormalizer(MLDataSet dataSet) {
        double minInput = 0;
        for (MLDataPair dataPair : dataSet) {
            for (int i = 0; i < dataPair.getInput().size(); i++) {
                if (dataPair.getInput().getData(i) < minInput) {
                    minInput = dataPair.getInput().getData(i);
                }
            }
        }
        return new NormalizedField(Normalize, "input", 0, minInput, 1, -1);
    }

    private List<NormalizedField> getIdealNormalizers(MLDataSet dataSet) {
        double minX = dataSet.collect { it.getIdeal().getData(0) }.min { it }
        double minY = dataSet.collect { it.getIdeal().getData(1) }.min { it }
        double minZ = dataSet.collect { it.getIdeal().getData(2) }.min { it }
        double maxX = dataSet.collect { it.getIdeal().getData(0) }.max { it }
        double maxY = dataSet.collect { it.getIdeal().getData(1) }.max { it }
        double maxZ = dataSet.collect { it.getIdeal().getData(2) }.max { it }

        List<NormalizedField> idealNormalizers = [
                new NormalizedField(Normalize, "x", maxX, minX, 1, -1),
                new NormalizedField(Normalize, "y", maxY, minY, 1, -1),
                new NormalizedField(Normalize, "z", maxZ, minZ, 1, -1),
        ]
        return idealNormalizers;
    }
}
