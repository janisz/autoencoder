# Data Extractor

    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python extractor.py <output_path> <path_to_data_dir>/*.csv