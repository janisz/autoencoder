#!/usr/bin/env python

import pandas as pd
import sys
import logging
import coloredlogs
import os
from numpy import random

SERIES = {'training': 1, 'validation': 3, 'test': 3}
DATA = {'gsm': '^gsm_rssi', 'wifi': '^wifigsm_rssi'}
COORDINATES_SUFFIX = {'local': '', 'global': '_global'}
COORDINATES_REGEX = {'local': '|^x$|^y$|^z$', 'global': '|^x_global$|^y_global$|^z$'}

logger = logging.getLogger('data-extractor')
coloredlogs.install(level=logging.DEBUG)

output_directory = sys.argv[1]
logger.info("Output directory %s", output_directory)

logger.info("Input %s", sys.argv[2:])
for filename in sys.argv[2:]:
    logger.debug("Reading data from " + filename)
    raw_data = pd.read_csv(filename)
    for data_type, data_type_regexp in DATA.iteritems():
        if not hasattr(raw_data, 'building'):
            logger.warning(filename + " has not proper schema")
            continue
        for building_id in raw_data.building.unique():
            for coords_name, coords_suffix in COORDINATES_SUFFIX.iteritems():
                data = raw_data.reindex(random.permutation(raw_data.index))
                data = raw_data.filter(regex='^serie$|' + data_type_regexp + COORDINATES_REGEX[coords_name])
                for series_name, series_id in SERIES.iteritems():
                    logger.info("Parsing %s %s data of building %s" % (series_name, data_type, building_id))
                    filtered_data = data[data.serie == series_id].filter(regex=data_type_regexp + COORDINATES_REGEX[coords_name])
                    filtered_data.fillna(0, inplace=True)
                    if series_name == 'validation':
                        filtered_data = filtered_data.tail(int(len(filtered_data)*0.2))
                    elif series_name == 'test':
                        filtered_data = filtered_data.head(int(len(filtered_data)*0.8))
                    columns = list(filtered_data.columns.values)
                    output_filename = "%s_%s_%s%s_%s" % (building_id,
                                                        data_type,
                                                        series_name,
                                                        coords_suffix,
                                                        os.path.basename(filename))
                    logger.info("Saving data to " + output_filename)
                    filtered_data.to_csv(os.path.join(output_directory, output_filename), index=False, columns=columns[3:] + ['x' + coords_suffix, 'y' + coords_suffix, 'z'])
