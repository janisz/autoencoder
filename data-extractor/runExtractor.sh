#!/usr/bin/env bash

outputPath=$1
inputFiles=$2

virtualenv venv && \
source venv/bin/activate && \
pip install -r requirements.txt && \
python extractor.py $outputPath $inputFiles