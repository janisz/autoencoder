AutoEncoder
----------------------

### Build and run

* Require Java 8
* Download data `./gradle downloadAndUnzipData` or for 2014 `./gradle extract2014Data -PsvnUsername=user -PsvnPassword=pass`
* Build `./gradlew build`
* Run long tests `./gradlew integTest`


### Docs

1. Install `pdflatex`, `graphviz` (on Ubuntu `sudo apt-get install texlive-full graphviz`)
2. Generate images `cd docs/images && dot -Tpng -O autoencoder_*.dot`
2. Run `./gradlew docks`

### Concatenate plots into one

    convert +append wifiXY.png wifiZY.png bottom.png && convert +append wifi.png wifiZX.png top.png && convert -append top.png bottom.png out.png && rm top.png bottom.png

### R
  * Install R on Ubuntu

        echo 'deb http://r.meteo.uni.wroc.pl/bin/linux/ubuntu trusty/' | sudo tee -a /etc/apt/sources.list.d/r.list
        sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
        sudo apt-get update
        sudo apt-get install r-base

  * Install packages (in R terminal `/usr/bin/R`)

        install.packages("hexbin")
        install.packages("gridExtra")
        install.packages("ggplot2")

  * Run scripts

        Rscript R/heatmap.r [input file (csv)] [output file (png)]
        Rscript R/histogram.r [input file (csv)] [output file (png)] [buckets count]

### Download results from external gird

        rsync -av -e ssh <user>@<host>:autoencoder/build/output/ ./

### Draw plots

* Error plot

        find <output_dir> -name 'olp_eksport_*_error*' -exec gnuplot -e "datapath='{}'; outputpath='{}.eps'" gnuplot/errorplot.gnuplot \;

* Stats

        find <output_dir> -name '*_predictor_*_set_error.csv' -exec Rscript R/stats.r "{}" "{}_stats.csv" "{}_stats.png" \;

* Histograms

        find <output_dir> -name '*_predictor_*_set_error.csv' -exec Rscript R/histogram.r "{}" "{}_histogram.png" <bucket count> \;

* Heatmaps

        find <output_dir> -name '*_predictor_*_set_error.csv' -exec Rscript R/heatmap.r "{}" "{}_heatmap.png" \;

* Visualisation

        dot -Tsvg -Ov <dot file>

* Stats & Whiskers plot

        find <output_dir> -name '*_predictor_*_set_error.csv' -exec Rscript R/stats.r "{}" "{}_stats.csv" \;
        find <output_dir> -name 'gsm_predictor_test_set_error.csv_stats.csv' -exec cat {} >> stats.csv \;
        sort -u -r -o stats.csv stats.csv
        Rscript R/whiskers.r stats.csv stats.pdf

