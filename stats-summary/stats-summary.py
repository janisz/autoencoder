#!/usr/bin/env python

import os
import re
import pandas as pd

input_path = "../build/output"
output_file = "stats_summary.csv"

interesting_headers = ["X - 90%", "Y - 90%", "Z - 90%"]

data = {'network conf': [],
        'building': [],
        'datetime': [],
        'type': [],
        'coords': [],
        'x 90%': [],
        'y 90%': [],
        'z 90%': []}
for root, dirs, files in os.walk(input_path):
    for file_name in files:
        mf = re.search(r"(test_set_error.csv_stats.csv$)", file_name)
        m = re.search(r"/build predictor for (?P<building>.+) "
                      r"of size \[(?P<network>.+)\] "
                      r"for (?P<type>\bwifi|gsm\b) "
                      r"(?P<coords>\bglobal|local\b) coords/(?P<date>\d+_\d+)", root)

        if mf is None or m is None:
            continue
        network_data = m.groupdict()
        data['network conf'].append(network_data['network'])
        data['building'].append(network_data['building'])
        data['datetime'].append(network_data['date'])
        data['type'].append(network_data['type'])
        data['coords'].append(network_data['coords'])

        raw_data = pd.read_csv(os.path.join(root, file_name), usecols=interesting_headers)
        data['x 90%'].append(raw_data[interesting_headers[0]][0])
        data['y 90%'].append(raw_data[interesting_headers[1]][0])
        data['z 90%'].append(raw_data[interesting_headers[2]][0])

data_frame = pd.DataFrame(data, columns=['network conf', 'building', 'datetime', 'type', 'coords', 'x 90%', 'y 90%', 'z 90%'])
data_frame.to_csv(os.path.join(input_path, output_file))





